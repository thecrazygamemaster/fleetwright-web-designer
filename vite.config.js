/* eslint-disable linebreak-style */
/* eslint-disable quotes */
import { URL, fileURLToPath } from "node:url";
import { execSync } from "node:child_process";

import { defineConfig } from "vite";
import electron from "vite-plugin-electron";
import vue from "@vitejs/plugin-vue";

const web_describe = execSync("git describe --dirty").toString().trim();
const meta_describe = execSync("git -C ./static/fleetwright-meta describe --always --dirty").toString().trim();

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
    if(mode==="electron") {
        return {
            build: {
                outDir: "dist-electron", // gitlab pages likes it this way
                sourcemap: true,
            },
            plugins: [
                vue(),
                electron({
                    entry: 'electron/main.ts',
                }),
            ],
            resolve: {
                alias: {
                    "@": fileURLToPath(new URL("./src", import.meta.url)),
                }
            },
            base: "./", // https://stackoverflow.com/a/71536031
            publicDir: "static",
            define: {
                __WEB_GIT_DESCRIBE__: JSON.stringify(web_describe),
                __META_GIT_DESCRIBE__: JSON.stringify(meta_describe),
            },
        };
    } else {
        return {
            build: {
                outDir: "public", // gitlab pages likes it this way
                sourcemap: true,
            },
            plugins: [
                vue()/*,
                electron({
                    entry: 'electron/main.ts',
                }),*/
            ],
            resolve: {
                alias: {
                    "@": fileURLToPath(new URL("./src", import.meta.url)),
                }
            },
            base: "./", // https://stackoverflow.com/a/71536031
            publicDir: "static",
            define: {
                __WEB_GIT_DESCRIBE__: JSON.stringify(web_describe),
                __META_GIT_DESCRIBE__: JSON.stringify(meta_describe),
            },
        };
    }
});
