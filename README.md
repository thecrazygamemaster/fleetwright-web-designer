# fleetwright-web-designer
Fleetwright Ship Designer written in Vue.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

## Project Setup

```sh
npm install
git submodule init
git submodule update
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```
## Contributing

- anything in issues is fair game; assign yourself or ask @SIGSTACKFAULT to do it
- if your commit is related to an issue start the message with `[#{issue number}]`
- grep the project for `#TODO` to find things to do.
  - #TODO: a proper todo list. or put them all in issues. or something
- PRs MUST pass all pipelines
  - ProTip: `eslint --fix`

## Vibe Sheet

- Comp/Con
- EVE in-game ship editor
  - PyFA
- NEBULOUS
