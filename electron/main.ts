import { BrowserWindow, app } from "electron";
import path from "path";

//let at = require('array.prototype.at');
//let assert = require('assert');

import { } from "./main.js";


app.whenReady().then(() => {
    const win = new BrowserWindow();

    if (process.env.VITE_DEV_SERVER_URL) {
        win.loadURL(process.env.VITE_DEV_SERVER_URL);
    } else {
        win.loadFile(path.join(__dirname, "index.html"));
    }
});