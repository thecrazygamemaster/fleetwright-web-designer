import { Component } from "./Component";
import { reactive } from "vue";
// model
import { data, data_promise } from "./globals";

export class Ship {
    constructor() {
        this.name = "";
        this.prefix = "";
        this.callsign = "";
        this.armour = 0;
        this.notes = "";
        /** @type {Component[]} */
        this.components = [];
        this.combat_stations = reactive({});
        data_promise.then(() => {
            for (let id in data.combat_stations) {
                this.combat_stations[id] = reactive({
                    biological: 0,
                    construct: 0,
                    infomorph: 0,
                    mechanical: 0,
                    brainwashed: 0,
                    any: 0,
                });
            }
        });
        this._faction_morale = 0; // assumed faction morale
    }

    load_ship(ship) {
        this.name = ship.name || "";
        this.prefix = ship.prefix || "";
        this.callsign = ship.callsign || "";
        this.armour = ship.armour || 0;
        this.author = ship.author || "";
        this.notes = ship.notes || "";
        this.components = [];
        for (let component of ship.components || []) {
            this.add_component(component);
        }
        for (let id in data.combat_stations) {
            //console.log("loading combat stations", id, ship.combat_stations?.[id], "->", this.combat_stations[id]);
            this.combat_stations[id].any /**********/ = ship.combat_stations?.[id]?.any ?? 0;
            this.combat_stations[id].biological /***/ = ship.combat_stations?.[id]?.biological ?? 0;
            this.combat_stations[id].construct /****/ = ship.combat_stations?.[id]?.construct ?? 0;
            this.combat_stations[id].infomorph /****/ = ship.combat_stations?.[id]?.infomorph ?? 0;
            this.combat_stations[id].mechanical /***/ = ship.combat_stations?.[id]?.mechanical ?? 0;
            this.combat_stations[id].brainwashed /**/ = ship.combat_stations?.[id]?.brainwashed ?? 0;
        }
        console.log("loaded ship", this);
    }

    add_component(component) {
        this.components.push(
            reactive(
                new Component(component.id, component.volume, component.armour, component.data)
            )
        );
    }

    remove_component(target) {
        let i = this.components.findIndex((element) => element._uuid == target._uuid);
        this.components.splice(i, 1);
    }

    *all_modules() {
        for (let component of this.components) {
            if (component.prototype() == undefined) {
                continue; // missing prototype
            }
            if (component.prototype().modules == null) {
                continue; // no modules
            }
            for (let [module_name, module_obj] of Object.entries(component.prototype().modules)) {
                yield [component, module_name, module_obj];

                if (module_name == "levelled_buff") {
                    //console.log(">> *all_modules: descending into levelled_buff", module_obj);
                    // get the selected level
                    let selected_level = component.data?.level ?? 1;
                    let submodule = module_obj.levels[selected_level ?? 1];
                    for (let [module_name, module_obj] of Object.entries(submodule)) {
                        yield [component, module_name, module_obj];
                    }
                }
            }
        }
    }

    /**
     *
     * @param {int} base_value
     * @param {string} what_buff
     */
    calculate_buff(what_buff) {
        let add = 0;
        let multiply = 1;
        let non_stacking = new Set();
        for (let [component, module_name, module_obj] of this.all_modules()) {
            if (module_name == "buff") {
                let base_add = module_obj?.add?.[what_buff] ?? 0;
                let base_mult = module_obj?.multiply?.[what_buff] ?? 1;
                if (non_stacking.has(component.id) && module_obj.stacking == false) {
                    continue;
                }
                non_stacking.add(component.id);
                switch (module_obj.scaling_function) {
                    case 1: // no scaling
                        add += base_add;
                        multiply *= base_mult;
                        break;
                    case undefined:
                    case "volume":
                        add += base_add * component.volume;
                        multiply *= base_mult ** component.volume;
                        break;
                    default:
                        console.warn("unknown buff.scaling_function", module_obj.scaling_function);
                        break;
                }
            }
        }
        return {
            add: add,
            multiply: multiply
        };
    }

    apply_buff(base_value, what_buff) {
        const buff = this.calculate_buff(what_buff);
        const result = (base_value + buff.add) * buff.multiply;
        console.debug(`apply_buff: ${what_buff} => +${buff.add} *${buff.multiply} =>`, result);
        return result;
    }

    // automatically made into a calculated
    /** @returns {int} */
    total_volume() {
        let sum = 0;
        for (let c of this.components) {
            sum += c.volume;
        }
        return sum;
    }

    /** @returns {int} */
    total_component_mass() {
        let sum = 0;
        for (let c of this.components) {
            sum += c.mass();
            //console.log(`total_component_mass: ${c.volume} -> ${sum}`);
        }
        return sum;
    }

    /** @returns {int} */
    total_cost() {
        let sum = this.armour * 0.2; // armour costs 1/mass
        for (let c of this.components) {
            sum += c.cost();
        }
        return sum;
    }

    /** @returns {int} */
    total_mass() {
        return this.total_component_mass() + this.armour;
    }

    /** @returns {int} */
    total_crew() {
        let sum = {
            strategic: 0,
            tactical: 0,
        };
        for (let [component, module_name, module_obj] of this.all_modules()) {
            if (module_name == "crew") {
                sum.strategic += (module_obj?.strategic?.base || 0)
                    + (module_obj?.strategic?.per_volume || 0) * component.volume;
                sum.tactical += (module_obj?.tactical?.base || 0)
                    + (module_obj?.tactical?.per_volume || 0) * component.volume;
            }
        }
        return sum;
    }

    /** @returns {int} */
    total_crew_worstcase() {
        if (this.components.length == 0) {
            return null;
        } else {
            return Math.max(...Object.values(this.total_crew()));
        }
    }

    /** @returns {int} */
    total_crew_quarters() {
        if (this.components.length == 0) {
            return null;
        }
        let sum = 0;
        for (let [component, module_name, module_obj] of this.all_modules()) {
            if (module_name == "quarters") {
                if (component.data?.occupied == true)
                    sum += module_obj.crew * component.volume;
            }
        }
        return sum;
    }


    /** @returns {Object.<string, int>} */
    total_tanks() {
        let sum = {};
        for (let [component, module_name, module_obj] of this.all_modules()) {
            if (module_name == "tank") {
                sum[module_obj.stores] = (sum[module_obj.stores] ?? 0) + component.volume * 10;
            }
        }
        return sum;
    }

    total_armoured_volume() {
        let sum = 0;
        for (let component of this.components) {
            if (component.armour)
                sum += component.volume;
        }
        return sum;
    }

    /** @returns {int} */
    protection() {
        return Math.floor(120 * (this.armour / Math.pow(this.total_armoured_volume(), 2 / 3)));
    }

    /** @returns {int} */
    best_jump() {
        let best = 0;
        for (let c of this.components) {
            if (c.prototype()?.type == "ftl_drive") {
                let range = c.jump_range(this.total_mass());
                if (range > best)
                    best = range;
            }
        }
        return best;
    }

    sensors() {
        return this.apply_buff(1, "sensors");
    }
}
