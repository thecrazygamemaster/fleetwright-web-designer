import { FLOATING_POINT_ROUNDING, data } from "./globals";
import { isReactive, toRaw } from "vue";
import { round10 } from "expected-round";
import { v1 as uuidv1 } from "uuid";

//////////////////////////////////////////////////////////////////////////////
// COMPONENT
//////////////////////////////////////////////////////////////////////////////

export class Component {
    constructor(id, volume = 1, armour = false, data = null) {
        this.id = id;
        this.volume = volume;
        this.armour = armour;
        this.data = data;
        this._uuid = uuidv1();
        this._simulation_state = {};
        if (this.prototype()?.fixed_volume) {
            this.volume = this.prototype().fixed_volume;
        }
    }

    /** @returns {{type:string,density:int,durability:int,modules:object,name:?string}} */
    prototype() {
        return data.components[this.id];
    }

    set_volume(new_volume) {
        //console.log("set_volume", this.volume, "->", new_volume);
        this.volume = new_volume;
    }

    toggle_armour() {
        this.armour = !this.armour;
    }

    set_data(data) {
        this.data = data;
    }

    /** @returns {int} */
    mass() {
        let mass = this.volume * (this.prototype()?.density ?? 1);
        let stored;
        for (let [module_name, module_obj] of Object.entries(this.prototype()?.modules ?? {})) {
            switch (module_name) {
                case "tank":
                    // #TODO handle partially-full tanks
                    stored = module_obj.stores;
                    if (stored == null) {
                        console.warn("no stored for tank", this);
                        break;
                    }
                    mass += data.resources[stored].density * this.volume;
                    break;
            }
        }
        return round10(mass, FLOATING_POINT_ROUNDING);
    }

    /** @returns {int} */
    cost() {
        return this.volume * (this.prototype()?.price ?? 1);
    }

    /** @returns {int} */
    crew() {
        return {
            strategic: (this.prototype()?.modules?.crew?.strategic?.base || 0) + (this.prototype()?.modules?.crew?.strategic?.per_volume || 0) * this.volume,
            tactical: (this.prototype()?.modules?.crew?.tactical?.base || 0) + (this.prototype()?.modules?.crew?.tactical?.per_volume || 0) * this.volume,
        };
    }

    /** @returns {int} */
    jump_range(total_mass) {
        const mass_fraction = this.mass() / total_mass;
        const jump_range = this.prototype().modules.ftl_drive.jump_range;
        const unrounded = (() => {
            switch (jump_range.method) {
                case "log":
                    // js doesn't seem to have log_n so use ln
                    return jump_range.constant + (Math.log(mass_fraction * jump_range.multiplier) / (Math.log(jump_range.base)));
                case "linear":
                    return jump_range.constant + mass_fraction * jump_range.multiplier;
                default:
                    console.warn("unknown ftl_drive.jump_range.method", jump_range.method);
                    return NaN;
            }
        })();
        console.log("jump_range", unrounded);
        return Math.floor(unrounded);
    }

    thrust() {
        return this.prototype().thrust * this.volume;
    }

    /** @param {Ship} parent_ship */
    available_fuel(parent_ship) {
        const relevant_tanks = parent_ship.components.filter(((c) => (c.prototype().type == "tank")
            && (c.prototype().stores == this.prototype().fuel)
        ));
        //console.log("available_fuel", relevant_tanks);
        return relevant_tanks.reduce((acc, component) => {
            return acc + component.volume * 10;
        }, 0);
    }

    /** @param {Ship} parent_ship */
    burn_time(parent_ship) {
        let fuel = this.available_fuel(parent_ship);
        return fuel / (this.prototype().prop_use * this.volume);
    }

    update_simulation_state(index, new_state) {
        console.log("update_simulation_state", this, ">>", index, toRaw(new_state));
        try {
            this._simulation_state[index] = new_state;
        } catch (ex) {
            console.log("update_simulation_state failed", ex, isReactive(this._simulation_state));
            return;
        }
        console.log("update_simulation_state success", toRaw(this));
    }

}
