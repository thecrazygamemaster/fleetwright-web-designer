import App from "./App.vue";
import { createApp } from "vue";

// icons
import "@mdi/font/css/materialdesignicons.css";
import "@fortawesome/fontawesome-free/css/all.css";
import { aliases, fa } from "vuetify/iconsets/fa";
import { mdi } from "vuetify/iconsets/mdi";

// vuetify
import "vuetify/styles";
import * as components from "vuetify/components";
import * as directives from "vuetify/directives";
import colors from "vuetify/lib/util/colors";
import { createVuetify } from "vuetify";

// needs to be below vuetify
import "./assets/main.less";
import "./assets/print.less";
// start loading
import "./globals.js";

console.log ("Main exists!");

const app = createApp(App);
const vuetify = createVuetify({
    components: components,
    directives: directives,
    display: {
        mobileBreakpoint: "md",
    },
    theme: {
        defaultTheme: "dark",
        themes: {
            dark: {
                dark: true,
                colors: {
                    background: "#222",
                    "on-background": "#fff",
                    surface: "#333",
                    primary: colors.red.darken1,
                    "primary-alt": colors.red.darken4,
                    secondary: colors.teal.base,
                    warning: colors.orange.darken3,
                    simulation: "#eee",
                    "on-simulation": "#000",
                    disabled: "#333",
                    "on-disabled": "#555",
                }
            },
            light: {
                dark: false,
                colors: {
                    background: "#ddd",
                    "on-background": "#000",
                    surface: "#ccc",
                    primary: colors.red.darken4,
                    "primary-alt": colors.red.darken1,
                    secondary: colors.teal.base,
                    warning: colors.amber.darken4,
                    "on-warning": colors.shades.black,
                    simulation: "#222",
                    "on-simulation": "#fff",
                    disabled: "#ccc",
                    "on-disabled": "#ddd",
                }
            }
        }
    },
    icons: {
        defaultSet: "fa",
        aliases,
        sets: { fa, mdi }
    }
});
app.use(vuetify);

app.mount("#app");