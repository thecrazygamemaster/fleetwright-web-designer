import { parse } from "yaml";
import { reactive } from "vue";

// round to this many decimal places in assorted places
// to avoid having like 0.30000000000000004
export const FLOATING_POINT_ROUNDING = -5;

// can't use await due to vue jank
export const data = reactive({});
export const data_promise = new Promise((resolve) => {
    fetch("fleetwright-meta/junctspace_dump.yaml").then(
        (value) => {
            value.text().then(
                (text) => {
                    let tmp = parse(text);
                    Object.assign(data, tmp);
                    console.log("got data dump", data);
                    resolve();
                }
            );
        }
    );
});

export const samples = reactive([]);
fetch("samples.yaml").then(
    (value) => {
        value.text().then(
            (text) => {
                let tmp = parse(text);
                for(let val of Object.values(tmp)){
                    samples.push(val);
                }
                console.log("got samples", samples);
            }
        );
    }
);

function round_away_from_zero(num) {
    if (num < 0)
        return Math.floor(num);
    else
        return Math.ceil(num);
}

function round_towards_zero(num) {
    if (num > 0)
        return Math.floor(num);
    else
        return Math.ceil(num);
}

