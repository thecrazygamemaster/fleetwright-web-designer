// eslint-disable-next-line no-unused-vars
import { Ship } from "./Ship";
import { toRaw } from "vue";

/**
 * combines the inputs/outputs of a converter, or the fuel/alternator of an engine, or etc.
 * to the resources object that the simulation expects
 * @param {Object.<string,int>} positives 
 * @param {Object.<string,int>} negatives 
 */
export function collect_resources(positives, negatives, volume) {
    positives = positives || {};
    negatives = negatives || {};
    let resources = {};
    for (let [resource, amount] of Object.entries(positives)) {
        resources[resource] = (resources[resource] ?? 0) + amount * volume;
    }
    for (let [resource, amount] of Object.entries(negatives)) {
        resources[resource] = (resources[resource] ?? 0) - amount * volume;
    }
    console.log("collect_resources", positives, negatives, resources);
    if(Object.values(collect_resources).includes(null))
        console.trace("collect_resources: part of result is null!", positives, negatives, "=>", resources);
    return resources;
}

/**
 * @param {Ship} ship
 * @param {str} which_resource
 * @returns {int} 
 */
export function simulate_resource_delta(ship, which_resource) {
    let delta = 0;
    for (let component of ship.components) {
        for (let simulated of Object.values(component._simulation_state)) {
            delta += simulated?.resources?.[which_resource] ?? 0;
        }
    }
    if (isNaN(delta))
        console.trace("simulate_resource_delta: result is NaN!", toRaw(ship), which_resource);
    return delta;
}

/**
 * @param {Ship} ship
 * @returns {double} 
 */
export function simulate_thrust(ship) {
    let sum = 0;
    for (let component of ship.components) {
        for (let simulated of Object.values(component._simulation_state || {})) {
            sum += simulated?.thrust ?? 0;
        }
    }
    return sum;
}

/**
 * @param {Ship} ship
 * @returns {double}
 */
export function simulate_acceleration(ship) {
    const base = simulate_thrust(ship) / ship.total_mass();
    if (base < 1) {
        // you don't have any engines
        // there's nothing to buff
        return 0;
    }
    return ship.apply_buff(base, "acceleration");
}

/**
 * @param {Ship} ship
 * @returns {double}
 */
export function simulate_ecm(ship) {
    const heat = 0; // simulate_resource_delta(ship, "HEAT");
    const volume = ship.total_volume();
    const raw_ecm = ship.apply_buff(0, "raw_ecm");
    const ecm = (1800 * raw_ecm) / (volume + heat);
    const buffed_ecm = ship.apply_buff(ecm, "ecm");
    console.log(`simulate_ecm: (heat=${heat}, volume=${volume}, raw_ecm=${raw_ecm}) => ${ecm} => ${buffed_ecm}`);
    return buffed_ecm;
}

/**
 * Ship's total morale
 * including faction morale
 * @param {Ship} ship 
 * @returns {int}
 */
export function simulate_morale(ship) {
    return ship._faction_morale;
}

/**
 * Ship's total Delta-V
 * @param {Ship} ship 
 * @returns {int}
 */
export function simulate_dv(ship) {
    return "?";
}